module.exports = function (eleventyConfig) {
  eleventyConfig.setTemplateFormats(["md", "html", "js", "css", "webp"]);

  eleventyConfig.ignores.add("src/web-components/**/*.html"); // Only needed in Liquid
  eleventyConfig.ignores.add("src/_pages");

  return {
    dir: { input: "src", output: "_site", data: "_data" },
  };
};
