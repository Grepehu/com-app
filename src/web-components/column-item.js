class ColumnItem extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.innerHTML = `
			<style>
			column-item {
				div {
					display: flex;
					flex-direction: column;
					gap: 1rem;
				}
				.check-group {
					display: flex;
					flex-direction: row;
				}
			}
			</style>
			<div class="column-item">
				<label>
					Type
					<select>
						<option>Logos</option>
						<option>Mythos</option>
					</select>
				</label>
				<label>
					Title
					<input type="text">
				</label>
				<div class="check-group">
					<label>Attention</label>
					<input type="checkbox">
					<input type="checkbox">
					<input type="checkbox">
				</div>
				<div class="check-group">
					<label>Fade/Crack</label>
					<input type="checkbox">
					<input type="checkbox">
					<input type="checkbox">
				</div>
				<label>
					Mystery/Identity
					<input type="text">
				</label>
			</div>
		`;
  }
}

window.customElements.define("column-item", ColumnItem);
