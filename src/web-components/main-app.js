class MainApp extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.innerHTML = `
			<web-link to="home">Home</web-link>
			<web-link to="test">Test</web-link>
			<web-router>
				<web-route path="home">
					<sheet-item></sheet-item>
				</web-route>
				<web-route path="test">
					<div>Test</div>	
				</web-route>
			</web-router>
		`;
  }
}

window.customElements.define("main-app", MainApp);
