class SheetItem extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    this.render();
  }

  render() {
    this.innerHTML = `
			<style>
				.sheet-item {
					display: flex;
					flex-direction: column;

					ul {
						display: flex;
						max-width: 100%;
						overflow: auto;
						flex-flow: row nowrap;
						scroll-snap-type: x mandatory;
						scroll-behavior: smooth;
						-webkit-overflow-scrolling: touch;

						li {
							min-width: 100%;
							scroll-snap-stop: always;
							scroll-snap-align: center;
							min-height: 300px; # test
						}
					}
				}
			</style>
			<div class="sheet-item">
				<nav>
					Char
				</nav>
				<ul class="app-scrollbar">
					<li><column-item></column-item></li>
					<li><column-item></column-item></li>
					<li><column-item></column-item></li>
					<li><column-item></column-item></li>
				</ul>
			</div>
		`;
  }
}

window.customElements.define("sheet-item", SheetItem);
