// Types

/**
 * @typedef {Object} WebNavigationEventChild
 * @property {Object} detail
 * @property {Object} detail.state
 * @property {string} detail.newRouterKey
 *
 * @typedef {Event & WebNavigationEventChild} WebNavigationEvent
 */

/**
 * @typedef {Object} FoundParam
 * @property {string} key
 * @property {string} value
 */

// Variable that saves the current page being displayed
let routerKey = "home";

// Utils Functions

/**
 * Pushes URL to a new location without reloading page
 * @param {string} newRouterKey RouterKey you want to navigate to
 * @param {*} state Any aditional data you may want to obtain back after the route has changed
 * @returns {void}
 */
function navigate(newRouterKey, state = {}) {
  const pushChangeEvent = new CustomEvent("webnavigation", {
    detail: {
      state: {
        previousRouterKey: routerKey,
        ...state,
      },
      newRouterKey,
    },
  });
  document.dispatchEvent(pushChangeEvent);
  routerKey = newRouterKey;
}

/**
 * Listens for the change in URL from the client-side navigation
 * @param {(e: WebNavigationEvent) => void} callback
 * @returns {void}
 */
function listenNavigation(callback) {
  document.addEventListener("webnavigation", callback, false);
}

// Components

class WebRouter extends HTMLElement {
  /** @type {FoundParam[]} */
  foundParams;

  constructor() {
    super();
  }

  connectedCallback() {
    this.updateActiveRoute(routerKey);
    listenNavigation((e) => {
      this.updateActiveRoute(e?.detail?.newRouterKey);
    });
  }

  /**
   * Updates which route is active or not
   * @param {string} newUrl
   */
  updateActiveRoute(newUrl) {
    let found = false;
    const routes = this.querySelectorAll("web-route");
    routes.forEach((thisRoute) => {
      // Reset all routes at first
      thisRoute.removeAttribute("active");

      // No need to search if routes has already been found
      if (found) return;
      const path = thisRoute?.getAttribute("path");

      // Exact match can return immediately
      if (newUrl === path) {
        thisRoute.setAttribute("active", "true");
        found = true;
        return;
      }
    });
  }
}

class WebRoute extends HTMLElement {
  /** @type {boolean} */
  active;

  constructor() {
    super();
    const active = this.getAttribute("active");
    this.active = active === "true";
  }

  connectedCallback() {
    this.updateRenderState();
  }

  static get observedAttributes() {
    return ["active"];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (oldValue === newValue) return;
    switch (name) {
      case "active":
        this.active = newValue === "true";
        break;
    }
    this.updateRenderState();
  }

  updateRenderState() {
    if (this.active) {
      this.classList.remove("hidden");
      // The follwing line is import to re-render the content
      // and activate any events that happen when mounting components
      this.innerHTML = this.innerHTML; // eslint-disable-line
    } else {
      this.classList.add("hidden");
    }
  }
}

class WebLink extends HTMLElement {
  /** @type {string} */
  to;

  constructor() {
    super();
    this.to = this.getAttribute("to");
  }

  connectedCallback() {
    this.tabIndex = 0;
    this.role = "link";
    this.style.cursor = "pointer";
    this.addEventListener("click", (e) => {
      e?.preventDefault?.();
      this.clickNavigate();
    });
  }

  clickNavigate() {
    navigate(this.to);
  }
}

// Definitions and Exports

window.customElements.define("web-router", WebRouter);
window.customElements.define("web-route", WebRoute);
window.customElements.define("web-link", WebLink);

export { navigate, listenNavigation, routerKey };
